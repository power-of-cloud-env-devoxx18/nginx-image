FROM nginx:stable

RUN apt-get update && \
    apt-get install -y curl && \
    # Comment user directive as master process is run as user in OpenShift anyhow
    sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf && \
    addgroup nginx root && \
    # To support arbitrary user IDs
    chgrp -R 0 /var/cache/nginx /var/run /var/log/nginx && \
    chmod -R g=u /var/cache/nginx /var/run /var/log/nginx && \
    mkdir -p /usr/src/app/dist && \
    echo "<!doctype html>" > /usr/src/app/dist/index.html && \
    # To support arbitrary user IDs
    chgrp -R 0 /usr/src/app && \
    chmod -R g=u /usr/src/app

COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080

USER 185
